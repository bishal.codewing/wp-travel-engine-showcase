<?php
/**
 * Plugin Name: WP Travel Engine - Showcase
 * Plugin URI: https://wptravelengine.com/
 * Description: Showcase your trips in a beautiful way.
 * Version: 1.0.0
 * Author: WP Travel Engine
 * Author URI: https://wptravelengine.com/
 * License: GPL2
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 * Text Domain: wp-travel-engine-showcase
 * Domain Path: /languages
 */

if( ! defined( 'ABSPATH' ) ) exit;

if( !class_exists( 'WP_Travel_Engine_Showcase' ) ) {
    require plugin_dir_path( __FILE__ ) . '/includes/class-showcase.php';
    function wpte_showcase_init() {
        return WP_Travel_Engine_Showcase\Plugin::instance();
    }
    add_action ( 'plugins_loaded', 'wpte_showcase_init' );
}