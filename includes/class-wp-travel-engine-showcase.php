<?php
class WPTE_Showcase_Admin
{

    function __construct()
    {
        add_action('init', array($this, 'register_shortcode'));
        add_action('init', array($this, 'showcase_post_type'));
        add_action('admin_menu', array($this, 'showcase_call_to_action_settings'));
        add_action('add_meta_boxes', array($this, 'showcase_add_meta_box'));
        add_action('save_post', array($this, 'showcase_save_meta_box'));
        add_action('admin_enqueue_scripts', array($this, 'showcase_admin_scripts'));
        add_action('wp_enqueue_scripts', array($this, 'showcase_scripts'));
        add_action('wp_ajax_wpte_showcase_load_more', array($this, 'wpte_showcase_load_more'));
        add_action('wp_ajax_nopriv_wpte_showcase_load_more', array($this, 'wpte_showcase_load_more'));
    }

    /**
     * Register Admin Styles and Scripts.
     */
    public function showcase_admin_scripts()
    {
        wp_register_style('showcase-admin-style', plugins_url('assets/css/admin-style.css', __FILE__));
        wp_enqueue_style('showcase-admin-style');
    }

    /**
     * Register Public Scripts and Styles
     */
    public function showcase_scripts()
    {
        wp_register_style('showcase-style',  plugins_url('assets/css/style.css', __FILE__));
        wp_enqueue_style('showcase-style');
        wp_register_script('main', plugins_url('assets/js/main.js', __FILE__), array('jquery'), '1.0.0', true);
        wp_localize_script('main', 'wpteshowcase', array('ajax_url' => admin_url('admin-ajax.php')));
        wp_enqueue_script('main');
    }

    /**
     * Register showcase post type
     */
    public function showcase_post_type()
    {
        $labels = array(
            'name' => _x('Showcase', 'wp_travel_engine_showcase'),
            'singular_name' => _x('Showcase', 'wp_travel_engine_showcase')
        );
        $args = array(
            'labels' => $labels,
            'show_in_menu' => true,
            'capability_type' => 'post',
            'menu_icon' => 'dashicons-format-gallery',
            'supports' => array('title', 'editor', 'author', 'thumbnail', 'excerpt'),
            'show_in_rest' => true,
            'public' => true,
            'publicly_queryable' => false,
        );
        register_post_type('wpteshowcase', $args);
    }

    /**
     * Call to Action Page
     */
    public function showcase_call_to_action_settings()
    {
        add_submenu_page(
            'edit.php?post_type=wpteshowcase',
            __('Showcase Settings', 'wp_travel_engine_showcase'),
            __('Showcase Settings', 'wp_travel_engine_showcase'),
            'manage_options',
            'showcase-settings',
            array($this, 'showcase_settings')
        );
    }

    /**
     * Call to Action Page Settings
     */
    public function showcase_settings()
    {
        if (array_key_exists('submit_call_to_action', $_POST)) {
            update_option('showcase_call_to_action', sanitize_text_field($_POST['showcase_call_to_action']));
            ?>
            <div id="message" class="updated notice is-dismissible">
                <p><strong><?php _e('Settings saved.') ?></strong></p>
                <button type="button" class="notice-dismiss">
                    <span class="screen-reader-text"><?php _e('Dismiss this notice.') ?></span>
                </button>
            </div>
        <?php
        }
        $showcase_call_to_action = get_option('showcase_call_to_action');
        ?>
        <div class="wrap">
            <h1 class="wp-heading-inline"><?php _e('Showcase Settings', 'wp_travel_engine_showcase'); ?></h1>
            <form method="POST" action="" class="wpte-showcase-form">
                <div class="wpte-showcase-meta-field">
                    <label class="wpte-showcase-input-label" for="wpte_showcase_redirect_url"><?php _e('Call to Action Text', 'wp_travel_engine_showcase'); ?></label>
                    <input class="wpte-showcase-input" type="text" name="showcase_call_to_action" value="<?php echo esc_attr($showcase_call_to_action); ?>" />
                </div>
                <button type="submit" name="submit_call_to_action" class="button button-primary button-large"><?php _e('Save', 'wp_travel_engine_showcase'); ?></button>
            </form>
        </div>
    <?php
    }

    /**
     * Add meta box
     */
    public function showcase_add_meta_box()
    {
        $screens = ['post', 'wpteshowcase'];
        foreach ($screens as $screen) {
            add_meta_box(
                'wpte_showcase_id',
                'WP Travel Engine Showcase',
                [self::class, 'showcase_meta_box_callback'],
                $screen
            );
        }
    }

    /**
     * Save meta box
     */
    public function showcase_save_meta_box($post_id)
    {
        if (isset($_POST['post_type']) && 'page' == $_POST['post_type']) {
            if (!current_user_can('edit_page', $post_id)) {
                return null;
            }
        } else {
            if (!current_user_can('edit_post', $post_id)) {
                return null;
            }
        }

        if (isset($_POST['wpte_showcase_redirect_url'])) {
            update_post_meta($post_id, 'wpte_showcase_redirect_url', esc_url_raw($_POST['wpte_showcase_redirect_url']));
        }
        $old = get_post_meta($post_id, 'wpte_showcase_addons_used', true);
        $new = array();
        $addons = $_POST['wpte_showcase_addons_used'];
        $count = count($addons);
        for ($i = 0; $i < $count; $i++) {
            if ($addons[$i] != '') :
                $new[$i]['wpte_showcase_addons_used'] = stripslashes(strip_tags($addons[$i]));
            endif;
        }
        if (!empty($new) && $new != $old)
            update_post_meta($post_id, 'wpte_showcase_addons_used', $new);
        elseif (empty($new) && $old)
            delete_post_meta($post_id, 'wpte_showcase_addons_used', $old);
    }

    /**
     * Meta box callback
     */
    public function showcase_meta_box_callback($post)
    {
        wp_nonce_field('wpte_showcase_meta_box', 'wpte_showcase_meta_box_nonce');
        $meta = get_post_meta($post->ID);
        ?>
        <div class="wpte-showcase-meta">
            <div>
                <h3 class="wpte-showcase-heading">WP Travel Engine Showcase Redirect Button</h3>
            </div>
            <div class="wpte-showcase-meta-row">
                <div class="wpte-showcase-meta-field">
                    <label class="wpte-showcase-input-label" for="wpte_showcase_redirect_url"><?php _e('Call to Action URL', 'wp_travel_engine_showcase'); ?></label>
                    <input class="wpte-showcase-input" type="text" name="wpte_showcase_redirect_url" id="wpte_showcase_redirect_url" value="<?php echo isset($meta['wpte_showcase_redirect_url']) ? esc_attr($meta['wpte_showcase_redirect_url'][0]) : ''; ?>">
                </div>
            </div>
            <div>
                <h3>WP Travel Engine Showcase Addons Used</h3>
            </div>
            <div class="wpte-showcase-meta-row">
                <?php
                $addons_data = wp_travel_engine_showcase_get_addons_from_store('addons');
                foreach ($addons_data as $product) {
                    $prod_info = $product->info;
                ?>
                    <label class="wpte-showcase-meta-addon" for="<?php echo $prod_info->slug; ?>">
                        <input id="<?php echo $prod_info->slug; ?>" type="checkbox" name="wpte_showcase_addons_used[]" value="<?php echo $prod_info->slug; ?>" />
                        <?php echo $prod_info->title; ?>
                    </label>
                    <?php
                }
                $meta_value = get_post_meta($post->ID, 'wpte_showcase_addons_used', true);
                if (!empty($meta_value)) {
                    foreach ($meta_value as $value) {
                    ?>
                        <script>
                            jQuery(document).ready(function($) {
                                $('input[name="wpte_showcase_addons_used[]"][value="<?php echo $value['wpte_showcase_addons_used']; ?>"]').attr('checked', true);
                            });
                        </script>
                <?php
                    }
                }
                ?>
            </div>
        </div>
        <?php
    }

    /**
     * Register shortcode.
     */
    public function register_shortcode()
    {
        add_shortcode('wpteshowcase', array($this, 'showcase_shortcode'));
    }

    /**
     * Shortcode to display showcase
     */
    function showcase_shortcode()
    {
        ob_start();
        $posts_per_page = get_option('posts_per_page', 10);
        $args = array(
            'post_type' => 'wpteshowcase',
            'post_status' => 'publish',
            'posts_per_page' => $posts_per_page,
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'paged' => get_query_var('paged') ? get_query_var('paged') : 1
        );
        $showcase = new WP_Query($args);
        if ($showcase->have_posts()) : ?>
            <div class="wpte-showcase">
                <div class="wpte-showcase-posts-row" id="wpte-showcase-posts">
                    <?php
                    while ($showcase->have_posts()) :
                        $showcase->the_post();
                        include __DIR__ . '/partials/content-showcase.php';
                    endwhile;
                    wp_reset_postdata();
                    ?>
                </div>
                <?php
                $count = $showcase->found_posts;
                if ($count > $posts_per_page) :
                ?>
                    <div class="wpte-showcase-load-more">
                        <button class="wpte-showcase-load-more-btn" data-current-page="<?php echo esc_attr($showcase->query_vars['paged']); ?>" data-total-page="<?php echo esc_attr($showcase->max_num_pages); ?>">Load More</button>
                        <input type="hidden" id="wpte_showcase_load_more_nonce" value="<?php echo wp_create_nonce('wpte_showcase_load_more_nonce'); ?>" />
                    </div>
                <?php endif; ?>
            </div>
            <?php
        endif;
        return ob_get_clean();
    }

    /**
     * Load more ajax callback
     */
    function wpte_showcase_load_more()
    {
        $nonce = (isset($_GET['nonce'])) ? $_GET['nonce'] : '';
        if (!wp_verify_nonce($nonce, 'wpte_showcase_load_more_nonce')) {
            wp_send_json_error('Permission denied');
        }
        ob_start();
        $posts_per_page = get_option('posts_per_page');
        $paged = (isset($_GET['paged'])) ? $_GET['paged'] : 1;
        $offset = $posts_per_page * ($paged - 1);
        $args = array(
            'post_type' => 'wpteshowcase',
            'post_status' => 'publish',
            'posts_per_page' => $posts_per_page,
            'orderby' => 'menu_order',
            'order' => 'ASC',
            'offset' => $offset,
        );
        $wpteshowcase = new WP_Query($args);
        if ($wpteshowcase->have_posts()) {
            while ($wpteshowcase->have_posts()) {
                $wpteshowcase->the_post();
                include __DIR__ . '/partials/content-showcase.php';
            }
        }
        wp_reset_postdata();
        $content = ob_get_clean();
        wp_send_json_success(['partials' => [
            '#wpte-showcase-posts' => [
                'type' => 'append',
                'content' => $content,
            ]
        ]]);
        die;
    }
}

new WPTE_Showcase_Admin();
