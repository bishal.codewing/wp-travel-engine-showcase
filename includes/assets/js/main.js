; (function ($) {
    var load_more = document.querySelector('.wpte-showcase-load-more-btn');
    if ( load_more ) {
        load_more.addEventListener('click', async function (e) {
            var nonce = document.querySelector('#wpte_showcase_load_more_nonce').value;
            e.preventDefault();
            e.target.classList.add('is-loading');
            var self = this;
            var currentPage = +self.dataset.currentPage
            var totalPage = +self.dataset.totalPage
            if (currentPage < totalPage) {
                var response = await fetch(wpteshowcase.ajax_url + '?action=wpte_showcase_load_more&paged=' + (currentPage + 1) + '&nonce=' + nonce)
                var result = await response.json();
                if (result.success) {
                    self.dataset.currentPage = currentPage + 1;
                    if(result.data.partials) {
                        var contents = Object.entries(result.data.partials)
                        contents.forEach(function (content) {
                            var el = document.querySelector(content[0])
                            var value = content[1]
                            if(el){
                                if(value.type == 'append') {
                                    el.insertAdjacentHTML('beforeend', value.content)
                                }
                            }
                        })
                        e.target.classList.remove('is-loading');
                    }
                    if( totalPage == currentPage + 1 ){
                        self.remove();
                    }
                }
                else {
                    e.target.classList.remove('is-loading');
                    console.log(result.data);
                }
            }
        });
    }
})();