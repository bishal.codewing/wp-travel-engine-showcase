<?php

function wp_travel_engine_showcase_get_addons_from_store( $type = 'addons' ) {
	$addons_data = get_transient( "wp_travel_engine_store_{$type}_list" );

	$links_by_type = (object) array(
		'addons'   => 'add-ons',
        'themes'   => 'themes',
	);

	if ( ! $addons_data ) {
		$addons_data = wp_safe_remote_get( WP_TRAVEL_ENGINE_STORE_URL . "/edd-api/v2/products/?category={$links_by_type->{$type}}&number=-1" );

		if ( is_wp_error( $addons_data ) ) {
			return;
		}

		$addons_data = wp_remote_retrieve_body( $addons_data );
		set_transient( "wp_travel_engine_store_{$type}_list", $addons_data, 48 * HOUR_IN_SECONDS );
	}

	if ( ! empty( $addons_data ) ) :

		$addons_data = json_decode( $addons_data );
		$addons_data = $addons_data->products;

	endif;

	return $addons_data;
}