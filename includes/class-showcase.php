<?php

namespace WP_Travel_Engine_Showcase;

final class Plugin {
    public $plugin_name = 'wpte-showcase';
    public $version = '1.0.0';
    public static $_instance = null;
    public static function instance() {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    private function includes() {
        require SHOWCASE_BASE_PATH . '/class-wp-travel-engine-showcase.php';
        require SHOWCASE_BASE_PATH . '/functions.php';
    }

    public function __construct() {
        $this->define_constants();
        $this->includes();
    }

    public function define_constants() {
        $this->define( 'SHOWCASE_VERSION', $this->version );
        $this->define( 'SHOWCASE_FILE_PATH', __FILE__ );
        $this->define( 'SHOWCASE_BASE_PATH', dirname(__FILE__) );
        $this->define( 'WP_TRAVEL_ENGINE_STORE_URL', 'https://wptravelengine.com' );
    }

    public function define( $name, $value ) {
        if(!defined($name)){
            define( $name, $value );
        }
    }
}