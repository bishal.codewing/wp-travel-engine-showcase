<?php
$wpte_showcase_button_url = get_post_meta( get_the_ID(), 'wpte_showcase_redirect_url', true );
?>

<div class="wpte-showcase-post-item">
    <div class="wpte-showcase-post">
        <?php 
        if(has_post_thumbnail()){ ?>
            <div class="wpte-showcase-post-thumbnail">
                <?php
                if( $wpte_showcase_button_url != '' ) {
                ?>
                    <a href="<?php echo esc_attr( $wpte_showcase_button_url ); ?>" rel="nofollow" target="_blank">
                        <?php the_post_thumbnail( 'full' ); ?>
                    </a>
                <?php
                } else { 
                    the_post_thumbnail( 'full' );
                } ?>
            </div>
        <?php } ?>
        <div class="wpte-showcase-post-content-wrap">
            <h3 class="wpte-showcase-post-title">
                <a href="<?php echo esc_attr( $wpte_showcase_button_url ); ?>" rel="nofollow" target="_blank">
                    <?php the_title(); ?>   
                </a>
            </h3>
            <div class="wpte-showcase-post-content">
                <?php the_excerpt(); ?>
            </div>
            <?php
            $addons_used = get_post_meta( get_the_ID(), 'wpte_showcase_addons_used', true );
            if( !empty( $addons_used ) ) {
            ?>
            <div class="wpte-showcase-post-addons">
                <h6>Addons Used</h6>
                <div class="wpte-showcase-post-addon-links">
                    <?php
                    foreach( $addons_used as $key => $value ) {
                        $product = wp_travel_engine_showcase_get_addons_from_store( 'addons' );
                        foreach( $product as $key => $prod ) {
                            $prod_info = $prod->info;
                            if( $prod_info->slug == $value['wpte_showcase_addons_used'] ) {
                            ?>
                                <a class="wpte-showcase-post-addon-link" href="<?php echo WP_TRAVEL_ENGINE_STORE_URL . $prod_info->permalink; ?>" target="_blank">
                                    <?php echo $prod_info->title; ?>
                                </a>
                            <?php
                            }
                        }
                    }
                    ?>
                </div>
            </div>
            <?php } ?>
            <div class="wpte-showcase-post-button-wrap">
                <?php
                if( $wpte_showcase_button_url != '') {
                    ?>
                    <a class="wpte-showcase-post-btn" href="<?php echo esc_attr( $wpte_showcase_button_url ); ?>" rel="nofollow" target="_blank">
                        <?php
                        $wpte_showcase_button_text = get_option( 'showcase_call_to_action' );
                        if( $wpte_showcase_button_text != '' ) {
                            echo esc_attr( $wpte_showcase_button_text );
                        } else {
                            echo 'View Demo';
                        }
                        ?>
                    </a>
                <?php } ?>
            </div>
        </div>
    </div>
</div>